# Smart POS with firebase
A POS system designed for restaurants and bars based on firebase and vueJS

## Project setup
```
run npm install to install all dependencies
create a firebase project from firebase.google.com
Replace the FirebaseSettings file in main.js with your own firebase config settings
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
See [Firebase docs](https://firebase.google.com/docs)
