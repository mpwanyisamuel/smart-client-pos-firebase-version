import * as firebase from 'firebase'
export default {
  namespaced: true,
  state: {
    users: [],
    userRoles: []
  },
  mutations: {
    setUsers (state, payload) {
      state.users = payload
    },
    setRoles (state, payload) {
      state.userRoles = payload
    }
  },
  actions: {
    updateUserStatus ({ dispatch }, payload) {
      const EMPLOYEES = firebase.firestore().collection('Accounts')
      EMPLOYEES.doc(payload.id).update(payload.update)
      dispatch('auth/setError', 'Success', { root: true })
    },
    updateEmployeeInfo ({ dispatch }, payload) {
      const EMPLOYEES = firebase.firestore().collection('Accounts')
      EMPLOYEES.doc(payload.id).update(payload.update)
      dispatch('auth/setError', 'Success', { root: true })
    },
    createEmployee ({ dispatch }, payload) {
      const EMPLOYEES = firebase.firestore().collection('Accounts')
      EMPLOYEES.where('email', '==', payload.email)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            EMPLOYEES.add(payload)
            dispatch('auth/setError', 'Sucess', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry, this email address is registered to a company', { root: true })
          }
        })
    },
    loadCompanyRoles ({ commit }) {
      const ROLES = firebase.firestore().collection('UserRoles')
      ROLES.orderBy('role', 'asc')
        .get()
        .then(userRoles => {
          const companyRoles = []
          userRoles.forEach(Role => {
            companyRoles.push({
              id: Role.id,
              role: Role.data().role
            })
          })
          commit('setRoles', companyRoles)
        })
    },
    loadUsers ({ commit }, companyId) {
      const EMPLOYEES = firebase.firestore().collection('Accounts')
      EMPLOYEES.where('company', '==', companyId)
        .orderBy('email', 'asc')
        .get()
        .then(users => {
          const allUsers = []
          users.forEach(user => {
            const userData = user.data()
            allUsers.push({
              id: user.id,
              email: userData.email,
              name: userData.name,
              outlet: userData.outlet,
              role: userData.role,
              status: userData.status
            })
          })
          commit('setUsers', allUsers)
        })
    }
  },
  getters: {
    employees: (state) => state.users,
    roles: (state) => state.userRoles
  }
}
