import * as firebase from 'firebase'
import router from '../../router'
import moment from 'moment'
moment.defaultFormat = 'DD.MM.YYYY HH:mm'

export default {
  namespaced: true,
  state: {
    loading: false,
    error: { status: false, message: '' },
    user: null,
    salesMenu: [],
    company: null,
    license: null
  },
  mutations: {
    setLicense (state, payload) {
      state.license = payload
    },
    setSalesMenu (state, payload) {
      state.salesMenu = payload
    },
    setCompany (state, payload) {
      state.company = payload
      const Comp = payload
      state.user.mobile = Comp.mobile
      state.user.company_status = Comp.status
      state.user.address = Comp.address
      state.user.type = Comp.type
      state.user.day_open = Comp.dayopen
      localStorage.setItem('day_open', Comp.dayopen)
    },
    setUser (state, payload) {
      state.user = payload
    },
    toggleLoad (state, payload) {
      state.loading = payload
    },
    toggleError (state, payload) {
      state.error = payload
    },
    notify (state, payload) {
      state.error = payload
    }
  },
  actions: {
    setError ({ commit }, payload) {
      commit('toggleError', { status: true, message: payload })
    },
    createNewAccount ({ commit }, payload) {
      commit('toggleLoad', true)
      const NewCompany = {
        dayopen: moment().format('DD-MM-YYYY'),
        name: payload.name,
        address: payload.address,
        mobile: payload.mobile,
        email: payload.email,
        type: payload.type,
        joined: moment().format('DD-MM-YYYY'),
        due_date: moment().day(30).format('DD-MM-YYYY'),
        status: false
      }
      const NewCompanyOutlet = {
        address: payload.address,
        name: 'MAIN',
        contact: payload.mobile,
        day_open: moment().format('DD-MM-YYYY')
      }
      const ACCOUNTS = firebase.firestore().collection('Clients')
      const USER_ACCOUNTS = firebase.firestore().collection('Accounts')
      const COMPANY_OUTLET = firebase.firestore().collection('Outlets')
      const LICENSES = firebase.firestore().collection('Licenses')
      ACCOUNTS.where('email', '==', payload.email)
        .get()
        .then(snapshots => {
          if (snapshots.size > 0) {
            commit('toggleLoad', false)
            commit('notify', { status: true, message: 'Sorry, THis Email Address Exists' })
          } else {
            ACCOUNTS.add(NewCompany)
              .then(doc => {
                const COMPANY_ID = doc.id
                const SUPER_ACCOUNT = {
                  name: payload.name,
                  email: payload.email,
                  role: 1,
                  company: doc.id
                }
                const COMPANY_LICENSE = {
                  start: moment().format('DD-MM-YYYY'),
                  due_date: moment().day(30).format('DD-MM-YYYY'),
                  company: doc.id,
                  key: 'STARTER'
                }
                localStorage.setItem('company', COMPANY_ID)
                LICENSES.where('company', '==', COMPANY_LICENSE.company)
                  .where('key', '==', COMPANY_LICENSE.key)
                  .get()
                  .then(snapshots => {
                    if (snapshots.size === 0) {
                      LICENSES.add(COMPANY_LICENSE)
                    }
                  })
                COMPANY_OUTLET.add({ ...NewCompanyOutlet, company: COMPANY_ID })
                  .then(() => {
                    USER_ACCOUNTS.add(SUPER_ACCOUNT)
                      .then(() => {
                        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
                          .then(user => {
                            localStorage.setItem('user', user.email)
                            router.push({ name: 'Sales' })
                          })
                          .catch(error => {
                            commit('toggleLoad', false)
                            commit('notify', { status: true, message: error.message })
                          })
                      })
                      .catch(error => {
                        commit('toggleLoad', false)
                        commit('notify', { status: true, message: error.message })
                      })
                  })
              })
              .catch(error => {
                commit('toggleLoad', false)
                commit('notify', { status: true, message: error.message })
              })
          }
        })
    },
    clearError ({ commit }) {
      commit('notify', { status: false, message: '' })
    },
    performLogout ({ commit }) {
      commit('toggleLoad', true)
      firebase.auth().signOut()
        .then(() => {
          localStorage.setItem('user', null)
          localStorage.setItem('company', null)
          localStorage.setItem('day_open', null)
          router.push({ name: 'login' })
          commit('toggleLoad', false)
        })
        .catch((error) => {
          commit('toggleLoad', false)
          commit('notify', { status: true, message: error.message })
        })
    },
    performPasswordReset ({ commit }, payload) {
      commit('toggleLoad', true)
      firebase.auth().sendPasswordResetEmail(payload)
        .then(() => {
          commit('toggleLoad', false)
          commit('notify', { status: true, message: 'A password reset link has been sent to your Email Address' })
        })
        .catch((error) => {
          commit('toggleLoad', false)
          commit('notify', { status: true, message: error.message })
        })
    },
    performLogin ({ commit, dispatch }, payload) {
      commit('toggleLoad', true)
      commit('notify', { status: false, message: '' })
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.pass)
        .then((user) => {
          dispatch('authUser', { email: user.user.email })
          commit('toggleLoad', false)
        })
        .catch((error) => {
          commit('toggleLoad', false)
          console.log('Auth Error', error.message)
          commit('notify', { status: true, message: error.message })
        })
    },
    getCompanyLicense ({ commit }, payload) {
      const LICENSES = firebase.firestore().collection('Licenses')
      LICENSES.where('company', '==', payload)
        .orderBy('due_date', 'desc')
        .limit(1)
        .get()
        .then(snapshots => {
          let LICENSE = Object()
          if (snapshots.size > 0) {
            snapshots.forEach(license => {
              const doc = license.data()
              LICENSE = {
                id: license.id,
                start: doc.start,
                ends: doc.due_date
              }
            })
            commit('setLicense', LICENSE)
          }
        })
    },
    getCompanyInfo ({ commit, state }, payload) {
      const COMPANY = firebase.firestore().collection('Clients')
      COMPANY.doc(payload)
        .get()
        .then((doc) => {
          if (doc.exists) {
            const Comp = doc.data()
            switch (payload.role) {
              case 1:
                router.push({ name: 'Sales' })
                break
              default:
                router.push({ name: 'Sales' })
                break
            }
            commit('setCompany', Comp)
          } else {
            commit('notify', 'No company found')
          }
        })
        .catch(error => {
          commit('notify', error.message)
        })
    },
    authUser ({ commit, dispatch }, payload) {
      commit('toggleLoad', true)
      if (payload) {
        const USER_ACCOUNTS = firebase.firestore().collection('Accounts')
        USER_ACCOUNTS.where('email', '==', payload.email)
          .where('status', '==', true).limit(1)
          .get()
          .then((snapshots) => {
            if (snapshots.size > 0) {
              const USER_PROFILE = Object()
              snapshots.forEach((doc) => {
                const profile = doc.data()
                USER_PROFILE.id = doc.id
                USER_PROFILE.email = profile.email
                USER_PROFILE.company = profile.company
                USER_PROFILE.name = profile.name
                USER_PROFILE.role = profile.role
                USER_PROFILE.status = profile.status
                USER_PROFILE.company = profile.company
                USER_PROFILE.outlet = profile.outlet
                localStorage.setItem('company', profile.company)

                commit('toggleLoad', false)
                dispatch('getCompanyInfo', profile.company)
                dispatch('getCompanyLicense', profile.company)
              })
              commit('setUser', USER_PROFILE)
              localStorage.user = USER_PROFILE.id
              if (USER_PROFILE.type === 'Restaurant' || USER_PROFILE.type === 'Bar') {
                commit('setSalesMenu', [{ tab: 'Dining' }, { tab: 'T-Away' }])
              } else {
                commit('setSalesMenu', [{ tab: 'Dining' }, { tab: 'T-Away' }])
              }
            } else {
              commit('toggleLoad', false)
              commit('notify', 'Sorry, no user information was found. Please Contact Admin')
            }
          })
          .catch((error) => {
            commit('notify', error.message)
          })
      } else {
        commit('setUser', null)
        router.push({ name: 'login' })
        commit('toggleLoad', false)
      }
    }
  },
  getters: {
    getError: (state) => state.error,
    loadingState: (state) => state.loading,
    profile: (state) => state.user,
    salesMenu: (state) => state.salesMenu,
    routes: (state) => {
      const routes = []
      if (state.user) {
        switch (state.user.role) {
          case 1:
            routes.push(
              { title: 'Dashboard', icon: 'mdi-chart-areaspline', page: '/admin' },
              { title: 'Sales', icon: 'mdi-home', page: '/Sales' },
              { title: 'Menu Items', icon: 'mdi-basket-outline', page: '/Menu' },
              { title: 'Tables And Sections', icon: 'mdi-menu', page: '/Sections' },
              { title: 'Outlets', icon: 'mdi-shopping', page: '/Outlets' },
              { title: 'My System Users', icon: 'mdi-account-group', page: '/Users' },
              { title: 'Settings', icon: 'mdi-cog', page: '#' }
              // { title: 'Reports', icon: 'mdi-chart-areaspline', page: '/Reports' },
            )
            break
          case 2:
            routes.push(
              { title: 'Sales', icon: 'mdi-home', page: '/Sales' }
            )
            break
          case 3:
            routes.push(
              { title: 'Sales', icon: 'mdi-home', page: '/Sales' }
            )
            break
        }
      }
      return routes
    },
    company: (state) => state.company,
    license: (state) => state.license
  }
}
