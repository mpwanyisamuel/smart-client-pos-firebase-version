import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import menu from './menu'
import sections from './sections'
import order from './order'
import manage from './manage'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    menu,
    sections,
    order,
    manage
  }
})
