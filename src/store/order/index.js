import * as firebase from 'firebase'
import moment from 'moment'
moment.defaultFormat = 'DD.MM.YYYY HH:mm'

export default {
  namespaced: true,
  state: {
    paymentTypes: [],
    clients: [],
    ordersToday: [],
    orderItems: [],
    loading: false,
    billPayments: []
  },
  mutations: {
    setPayments (state, payload) {
      state.billPayments = payload
    },
    toggleLoading (state, payload) {
      state.loading = payload
    },
    setOrderItems (state, payload) {
      state.orderItems = payload
    },
    setOrders (state, payload) {
      state.ordersToday = payload
    },
    setClients (state, payload) {
      state.clients = payload
    },
    setPaymentTypes (state, payload) {
      state.paymentTypes = payload
    }
  },
  actions: {
    deletePayment ({ commit }, payload) {
      firebase.firestore().collection('Payments').doc(payload).delete()
    },
    loadPayments ({ commit }, payload) {
      const PAYMENTS = firebase.firestore().collection('Payments')
      PAYMENTS.where('company', '==', payload.company)
        .where('date', '==', payload.day_open)
        .get()
        .then(snapshots => {
          const allPayments = []
          snapshots.forEach((payment) => {
            const pay = payment.data()
            allPayments.push({
              id: payment.id,
              amount: pay.amount,
              outlet: pay.outlet,
              client: pay.client,
              date: pay.date,
              order: pay.order,
              payment: pay.payment,
              reason: pay.reason,
              settledby: pay.settledby
            })
          })
          commit('setPayments', allPayments)
        })
        .catch(error => {
          console.log('Error Fetching Payments', error.message)
        })
    },
    splitSettlement ({ commit }, payload) {
      commit('toggleLoading', true)
      const ORDER_SETTLEMENTS = firebase.firestore().collection('Payments')
      const ORDER_REF = firebase.firestore().collection('ClientOrders').doc(payload.order)
      payload.payments.forEach((payment) => {
        const splitPayment = {
          payment: payment.id,
          amount: payment.amount,
          client: payload.client,
          order: payload.order,
          date: payload.date,
          settledby: payload.settledby,
          company: payload.company,
          reason: payload.reason
        }
        if (payment.amount > 0) {
          ORDER_SETTLEMENTS
            .where('order', '==', payload.order)
            .where('payment', '==', payment.id)
            .get()
            .then(snapshots => {
              if (snapshots.size === 0) {
                ORDER_SETTLEMENTS.add(splitPayment)
              }
            })
        }
      })
      ORDER_REF.update({ settlement: 'SPLIT PAYMENT', status: 1 })
    },
    settleBill ({ dispatch }, payload) {
      const ORDER_PAYMENTS = firebase.firestore().collection('Payments')
      const ORDER_REF = firebase.firestore().collection('ClientOrders').doc(payload.order)
      if (payload.payment === '88') { // Discount
        ORDER_PAYMENTS.where('order', '==', payload.order)
          .where('payment', '==', payload.payment)
          .limit(1)
          .get()
          .then(snapshots => {
            if (snapshots.size === 0) {
              ORDER_PAYMENTS.add(payload)
            } else {
              snapshots.forEach((docRef) => {
                ORDER_PAYMENTS.doc(docRef.id).update(payload)
              })
            }
          })
      } else {
        ORDER_PAYMENTS.where('order', '==', payload.order)
          .get()
          .then(snapshots => {
            let settled = false
            snapshots.forEach((payment) => {
              if (payment.data().payment !== '88') {
                settled = true
              }
            })
            if (!settled) {
              ORDER_PAYMENTS.add(payload)
              ORDER_REF.update({ settlement: payload.payment, status: 1 })
            } else {
              dispatch('auth/setError', 'Order Settled already', { root: true })
            }
          })
      }
    },
    confirmOrder ({ dispatch }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems')
      ORDER_ITEMS.where('order', '==', payload)
        .get()
        .then(snapshots => {
          if (snapshots.size > 0) {
            snapshots.forEach(orderItem => {
              ORDER_ITEMS.doc(orderItem.id).update({ status: 1 })
            })
          }
        })
    },
    addItemDiscount ({ commit }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems').doc(payload.recordId)
      const DISCOUNTS = firebase.firestore().collection('Discounts')
      const itemDiscount = {
        company: payload.company,
        menuitem: payload.menuitem,
        date: payload.date,
        addedby: payload.addedby,
        order: payload.orderId,
        amount: payload.amount,
        description: payload.discounttype,
        reason: payload.reason
      }
      const ITEM_UPDATE = {
        amount: payload.finalamount
      }
      DISCOUNTS
        .where('menuitem', '==', payload.menuitem)
        .where('order', '==', payload.orderId)
        .limit(1)
        .get()
        .then(snapshots => {
          ORDER_ITEMS.update(ITEM_UPDATE)
          if (snapshots.size === 0) {
            DISCOUNTS.add(itemDiscount)
          } else {
            snapshots.forEach(data => {
              const docId = data.id
              DISCOUNTS.doc(docId).update(itemDiscount)
            })
          }
        })
    },
    deleteOrderItem ({ commit }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems')
      const DISCOUNTS = firebase.firestore().collection('Discounts')
      ORDER_ITEMS.doc(payload.id).delete()
      DISCOUNTS
        .where('menuitem', '==', payload.menuitem)
        .where('order', '==', payload.order)
        .where('date', '==', payload.date)
        .limit(1)
        .get()
        .then(snapshots => {
          if (snapshots.size > 0) {
            snapshots.forEach(doc => {
              DISCOUNTS.doc(doc.id).delete()
            })
          }
        })
    },
    updateOrderItem ({ commit }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems')
      const update = {
        unitprice: payload.unitprice,
        quantity: payload.quantity,
        amount: payload.amount
      }
      ORDER_ITEMS.doc(payload.id).update(update)
    },
    fetchOrderItems ({ commit }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems')
      ORDER_ITEMS.where('company', '==', payload.company)
        .where('date', '==', payload.day)
        .orderBy('status', 'asc')
        .get()
        .then(snapshots => {
          const mOrderItems = []
          snapshots.forEach(item => {
            const mItem = item.data()
            mOrderItems.push({
              id: item.id,
              order: mItem.order,
              menuitem: mItem.item,
              unitprice: mItem.unitprice,
              quantity: mItem.quantity,
              amount: mItem.amount,
              date: mItem.date,
              time: mItem.time,
              status: mItem.status,
              addedby: mItem.addedby
            })
          })
          commit('setOrderItems', mOrderItems)
        })
    },
    addOrderItem ({ dispatch }, payload) {
      const ORDER_ITEMS = firebase.firestore().collection('OrderItems')
      ORDER_ITEMS.where('order', '==', payload.order)
        .where('item', '==', payload.item)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            ORDER_ITEMS.add(payload)
          } else {
            dispatch('auth/setError', 'Sorry, You Already Added This Item', { root: true })
          }
        })
        .catch(error => {
          dispatch('auth/setError', error.message, { root: true })
        })
    },
    loadCLients ({ commit }) {
      const COMPANY_ID = localStorage.getItem('company')
      const CLIENTS = firebase.firestore().collection('POSClients')
      CLIENTS.where('company', '==', COMPANY_ID).orderBy('firstname', 'asc')
        .get()
        .then(snapshots => {
          const mClients = []
          snapshots.forEach(client => {
            const data = client.data()
            mClients.push({
              id: client.id,
              firstname: data.firstname,
              lastname: data.lastname,
              address: data.address,
              tin: data.tin,
              contact: data.contact,
              email: data.email,
              created: data.created
            })
          })
          commit('setClients', mClients)
        })
    },
    loadPaymentTypes ({ commit }) {
      const SETTLEMENTS = firebase.firestore().collection('settlementTypes')
      SETTLEMENTS.orderBy('settlement', 'asc')
        .get()
        .then(snapshots => {
          const mSettlements = []
          snapshots.forEach((settlement) => {
            const payType = settlement.data()
            mSettlements.push({
              id: settlement.id,
              ref: payType.ref,
              settlement: payType.settlement
            })
          })
          commit('setPaymentTypes', mSettlements)
        })
    },
    createNewClient ({ commit, dispatch }, payload) {
      const CLIENTS = firebase.firestore().collection('POSClients')
      const client = {
        firstname: payload.firstname,
        lastname: payload.lastname,
        address: payload.address,
        tin: payload.tin,
        contact: payload.contact,
        email: payload.email,
        company: payload.company,
        created: payload.created
      }
      const order = {
        table: 'T-Away',
        time: payload.time,
        createdby: payload.createdby,
        status: 0,
        settlement: 0,
        date: payload.date,
        company: payload.company
      }
      CLIENTS.where('firstname', '==', payload.firstname)
        .where('lastname', '==', payload.lastname)
        .where('address', '==', payload.address)
        .where('company', '==', payload.company)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            CLIENTS.add(client)
              .then(doc => {
                const newClient = {
                  ...order,
                  client: doc.id
                }
                if (payload.action === 2) {
                  dispatch('order/createOrder', newClient, { root: true })
                } else {
                  dispatch('auth/setError', 'Success ' + payload.firstname + ' Was Added!', { root: true })
                }
              })
          } else {
            dispatch('auth/setError', 'Sorry, ' + payload.firstname + ' Already Exists', { root: true })
          }
        })
    },
    loadOrders ({ commit }, payload) {
      const COMPANY_ID = payload.company || localStorage.getItem('company')
      const DAY_OPEN = payload.day || localStorage.getItem('day_open')
      const ORDERS = firebase.firestore().collection('ClientOrders')
      ORDERS.where('company', '==', COMPANY_ID)
        .where('date', '==', DAY_OPEN)
        .orderBy('bill', 'desc')
        .get()
        .then(snapshots => {
          const mOrders = []
          snapshots.forEach(order => {
            const info = order.data()
            mOrders.push({
              id: order.id,
              bill: info.bill,
              company: info.company,
              createdby: info.createdby,
              date: info.date,
              client: info.client,
              settlement: info.settlement,
              section: info.section,
              status: info.status,
              table: info.table,
              time: info.time
            })
          })
          commit('setOrders', mOrders)
        })
    },
    createOrder ({ commit, dispatch }, payload) {
      const ORDERS = firebase.firestore().collection('ClientOrders')
      ORDERS.where('company', '==', payload.company)
        .get()
        .then(snapshots => {
          const orderNumber = snapshots.size
          const newOrder = {
            ...payload,
            bill: parseFloat(orderNumber + 1)
          }
          ORDERS.add(newOrder)
        })
    }
  },
  getters: {
    paymentTypes: (state) => state.paymentTypes,
    dashboardPaymentOverview: (state) => state.paymentTypes.filter(Payment => { return Payment.settlement !== 'NOT CHARGABLE' && Payment.settlement !== 'CANCELLED' }),
    creditSettlement: (state) => state.paymentTypes.filter(Payment => { return Payment.settlement === 'CREDIT' }),
    clients: (state) => state.clients,
    allOrders: (state) => state.ordersToday,
    ordersToday: (state) => state.ordersToday.filter((order) => { return order.status === 0 }),
    orderItems: (state) => state.orderItems,
    pendingOrders: (state) => state.ordersToday.filter((order) => { return order.status === 0 }),
    settledOrders: (state) => state.ordersToday.filter((order) => { return order.status !== 0 }),
    allPayments (state) {
      return state.billPayments// .filter((Payment) => { return Payment.date === localStorage.getItem('day_open') })
    },
    creditors (state) {
      const creditId = state.paymentTypes.find(Payment => { return Payment.settlement === 'CREDIT' })
      let creditors = []
      if (creditId) {
        creditors = state.billPayments.filter((payment) => { return payment.payment === creditId.id })
      }
      return creditors
    },
    paymentsToday: (state) => (dayOpen) => { return state.billPayments.filter((Payment) => { return Payment.date === dayOpen }) },
    discounts: (state) => state.billPayments.filter((payment) => { return payment.payment === '88' })
  }
}
