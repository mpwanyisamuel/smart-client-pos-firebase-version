import * as firebase from 'firebase'
import moment from 'moment'
moment.defaultFormat = 'DD.MM.YYYY HH:mm'

export default {
  namespaced: true,

  state: {
    sections: [],
    tables: [],
    outlets: []
  },

  mutations: {
    setOutlets (state, payload) {
      state.outlets = payload
    },
    setSections (state, payload) {
      state.sections = payload
    },
    setTables (state, payload) {
      state.tables = payload
    }
  },

  actions: {
    updateOutlet ({ dispatch }, payload) {
      const OUTLET_UPDATE = {
        name: payload.name,
        address: payload.address,
        contact: payload.contact
      }
      const OUTLETS = firebase.firestore().collection('Outlets')
      OUTLETS
        .where('company', '==', payload.company)
        .where('name', '==', payload.name)
        .where('address', '==', payload.address)
        .where('contact', '==', payload.contact)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            OUTLETS.doc(payload.id).update(OUTLET_UPDATE)
            dispatch('auth/setError', 'Outlet was updated', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry ' + payload.name + ' Already Exists!', { root: true })
          }
        })
    },
    createNewOutlet ({ dispatch }, payload) {
      const OUTLETS = firebase.firestore().collection('Outlets')
      OUTLETS.where('company', '==', payload.company).where('name', '==', payload.name).where('address', '==', payload.address)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            OUTLETS.add({ ...payload, day_open: moment().format('DD-MM-YYYY') })
            dispatch('auth/setError', 'Outlet was created', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry ' + payload.name + ' Already Exists!', { root: true })
          }
        })
    },
    closeCompanyDay ({ dispatch }, payload) {
      const OUTLETS = firebase.firestore().collection('Outlets')
      OUTLETS.doc(payload.outlet)
        .get()
        .then(doc => {
          if (doc.exists) {
            OUTLETS.doc(payload.outlet).update({ day_open: payload.day_selected })
            dispatch('auth/setError', `Success ${payload.day_selected} is now open!`, { root: true })
          }
        })
    },
    getOutlets ({ commit }, companyId) {
      const OUTLETS = firebase.firestore().collection('Outlets')
      if (companyId) {
        OUTLETS.where('company', '==', companyId)
          .get()
          .then(outlets => {
            const allOutlets = []
            outlets.forEach(outlet => {
              const mOutlet = outlet.data()
              allOutlets.push({
                id: outlet.id,
                name: mOutlet.name,
                address: mOutlet.address,
                contact: mOutlet.contact,
                company: mOutlet.company,
                day_open: mOutlet.day_open
              })
            })
            commit('setOutlets', allOutlets)
          })
      }
    },
    showRestaurantTable ({ dispatch }, payload) {
      const TABLES = firebase.firestore().collection('Tables')
      TABLES.doc(payload).update({ hidden: false })
    },
    hideRestaurantTable ({ dispatch }, payload) {
      const TABLES = firebase.firestore().collection('Tables')
      TABLES.doc(payload).update({ hidden: true })
    },
    updateRestaurantTable ({ dispatch }, payload) {
      const TABLES = firebase.firestore().collection('Tables')
      const updated = {
        table: payload.table,
        section: payload.section
      }
      TABLES.where('section', '==', payload.section).where('table', '==', payload.table)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            TABLES.doc(payload.id).update(updated)
          } else {
            dispatch('auth/setError', 'Sorry, You Already Have ' + payload.table + ' In This Section', { root: true })
          }
        })
    },
    getTables ({ commit }) {
      const TABLES = firebase.firestore().collection('Tables')
      const COMPANY_ID = localStorage.getItem('company')
      TABLES.where('company', '==', COMPANY_ID).orderBy('table', 'asc')
        .get()
        .then(snapshots => {
          const mTables = []
          snapshots.forEach(table => {
            mTables.push({
              id: table.id,
              section: table.data().section,
              table: table.data().table,
              hidden: table.data().hidden
            })
          })
          commit('setTables', mTables)
        })
    },
    createNewTable ({ dispatch }, payload) {
      const TABLES = firebase.firestore().collection('Tables')
      TABLES.where('table', '==', payload.table).where('section', '==', payload.section)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            TABLES.add(payload)
          } else {
            dispatch('auth/setError', 'SORRY, ' + payload.table + ' Exists', { root: true })
          }
        })
    },
    createSection ({ dispatch }, payload) {
      const SECTIONS = firebase.firestore().collection('Sections')
      SECTIONS.where('section', '==', payload.section)
        .where('company', '==', payload.company)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            SECTIONS.add(payload).then(() => { dispatch('auth/setError', 'Success', { root: true }) })
          } else {
            dispatch('auth/setError', 'SORRY, ' + payload.section + ' ALREADY EXISTS', { root: true })
          }
        })
    },
    getSections ({ commit }) {
      const SECTIONS = firebase.firestore().collection('Sections')
      const COMPANY_ID = localStorage.getItem('company')
      SECTIONS.where('company', '==', COMPANY_ID).orderBy('section', 'asc')
        .get()
        .then(sections => {
          const mSections = []
          sections.forEach(section => {
            mSections.push({
              id: section.id,
              section: section.data().section,
              company: section.data().company,
              hidden: section.data().hidden
            })
          })
          commit('setSections', mSections)
        })
    },
    updateSectionDetails ({ commit, dispatch }, payload) {
      const SECTIONS = firebase.firestore().collection('Sections')
      SECTIONS.where('company', '==', payload.company)
        .where('section', '==', payload.section)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            SECTIONS.doc(payload.id).update({ section: payload.section })
            this.dispatch('auth/setError', 'SUCCESS!', { root: true })
          } else {
            this.dispatch('auth/setError', 'SORRY, SECTION EXISTS', { root: true })
          }
        })
    },
    hideSection ({ dispatch }, payload) {
      const SECTIONS = firebase.firestore().collection('Sections')
      SECTIONS.doc(payload.id).update({ hidden: true })
    },
    showSection ({ dispatch }, payload) {
      const SECTIONS = firebase.firestore().collection('Sections')
      SECTIONS.doc(payload.id).update({ hidden: false })
    }
  },

  getters: {
    loadedSections: (state) => state.sections,
    loadedTables: (state) => state.tables,
    outlets: (state) => state.outlets
  }
}
