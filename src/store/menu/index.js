import * as firebase from 'firebase'
export default {
  namespaced: true,
  state: {
    categories: [],
    menuItems: [],
    selectedMenuCategory: 0
  },
  mutations: {
    setSelectedCategoryId (state, payload) {
      state.selectedMenuCategory = payload
    },
    setCategories (state, payload) {
      state.categories = payload
    },
    setMenuItems (state, payload) {
      state.menuItems = payload
    }
  },
  actions: {
    searchMenuByCategory ({ commit }, payload) {
      commit('setSelectedCategoryId', payload)
    },
    hideOrShowMenuItem ({ dispatch }, payload) {
      const MENU_ITEMS = firebase.firestore().collection('menuItems')
      MENU_ITEMS.doc(payload.id).update({ hidden: payload.hidden })
      dispatch('auth/setError', 'Success', { root: true })
    },
    loadMenu ({ commit }) {
      const COMPANY_ID = localStorage.getItem('company')
      const MENU_ITEMS = firebase.firestore().collection('menuItems')
      MENU_ITEMS.where('company', '==', COMPANY_ID)
        .get()
        .then(menuItems => {
          const menu = []
          menuItems.forEach(item => {
            menu.push({
              id: item.id,
              company: item.data().company,
              name: item.data().name,
              price: item.data().price,
              category: item.data().category,
              description: item.data().description,
              hidden: item.data().hidden,
              outlet: item.data().outlet,
              code: item.data().code
            })
          })
          commit('setMenuItems', menu)
        })
    },
    updateMenuItem ({ dispatch }, payload) {
      const updatedItem = {
        code: payload.code,
        name: payload.name,
        price: payload.price,
        category: payload.category,
        description: payload.description
      }
      const MENU_ITEMS = firebase.firestore().collection('menuItems')
      MENU_ITEMS.where('name', '==', updatedItem.name)
        .where('code', '==', payload.code)
        .where('company', '==', payload.company)
        .where('category', '==', updatedItem.category)
        .where('description', '==', payload.description)
        .where('price', '==', payload.price)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            MENU_ITEMS.doc(payload.id).update(updatedItem)
            dispatch('auth/setError', 'Success, ' + payload.name + ' Was Updated!', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry, ' + payload.name + ' Already Exists', { root: true })
          }
        })
        .catch(error => {
          dispatch('auth/setError', error.message, { root: true })
        })
    },
    createNewMenuItem ({ dispatch }, payload) {
      const MENU_ITEMS = firebase.firestore().collection('menuItems')
      MENU_ITEMS.where('name', '==', payload.name)
        .where('company', '==', payload.company)
        .where('category', '==', payload.category)
        .where('outlet', '==', payload.outlet)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            MENU_ITEMS.add(payload)
            dispatch('auth/setError', 'Success, ' + payload.name + ' Was Added!', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry, ' + payload.name + ' Already Exists', { root: true })
          }
        })
    },
    updateCategoryItem ({ dispatch }, payload) {
      const CATEGORIES = firebase.firestore().collection('menuCategories')
      CATEGORIES
        .where('category', '==', payload.category)
        .where('company', '==', payload.company)
        .where('outlet', '==', payload.outlet)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            CATEGORIES.doc(payload.id).update({ category: payload.category, outlet: payload.outlet })
            dispatch('auth/setError', 'Success!', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry, There is already a category ' + payload.category, { root: true })
          }
        })
    },
    deleteMenuCategory ({ dispatch }, payload) {
      const MENU_ITEMS = firebase.firestore().collection('menuItems')
      const CATEGORIES = firebase.firestore().collection('menuCategories')

      MENU_ITEMS.where('category', '==', payload.id)
        .where('company', '==', payload.company)
        .get()
        .then(snapshots => {
          if (snapshots.size === 0) {
            CATEGORIES.doc(payload.id).delete()
            dispatch('auth/setError', 'Deleted Successfully', { root: true })
          } else {
            dispatch('auth/setError', 'Sorry, You have Menu Items Attached To This Category', { root: true })
          }
        })
    },
    loadCategories ({ commit }) {
      const CATEGORIES = firebase.firestore().collection('menuCategories')
      const COMPANY_ID = localStorage.getItem('company')
      CATEGORIES.where('company', '==', COMPANY_ID).orderBy('category', 'asc')
        .get()
        .then((snapshots) => {
          const mCategories = []
          snapshots.forEach((category) => {
            mCategories.push({
              id: category.id,
              category: category.data().category,
              company: category.data().company,
              outlet: category.data().outlet
            })
          })
          commit('setCategories', mCategories)
        })
    },
    createCategory ({ dispatch }, payload) {
      const CATEGORIES = firebase.firestore().collection('menuCategories')
      CATEGORIES
        .where('category', '==', payload.category)
        .where('company', '==', payload.company)
        .get()
        .then((snapshots) => {
          if (snapshots.size === 0) {
            CATEGORIES.add(payload)
            dispatch('auth/setError', 'Success!', { root: true })
          } else {
            dispatch('auth/setError', `Sorry ${payload.category} Already Exists`, { root: true })
          }
        })
    },
    async checkMenuCategory ({ commit }, payload) {
      const CATEGORIES = firebase.firestore().collection('menuCategories')
      return await CATEGORIES
        .where('category', '==', payload.category)
        .where('company', '==', payload.company)
        .get()
        .then(snapshots => {
          let categoryRetrived
          if (snapshots.size === 0) {
            CATEGORIES.add(payload)
              .then(doc => {
                categoryRetrived = doc.id
              })
              .catch(error => { console.log(error.message) })
          } else {
            snapshots.forEach(cat => {
              categoryRetrived = cat.id
            })
          }
          return categoryRetrived
        })
    }
  },
  getters: {
    menuCategories: (state) => state.categories,
    menuItems: (state) => state.menuItems,
    menuItemsByCategory: (state) => {
      const activeManuItems = state.menuItems.filter((item) => { return item.hidden === false })
      let filteredList
      if (state.selectedMenuCategory === 0) filteredList = activeManuItems
      else if (state.selectedMenuCategory !== 0) filteredList = activeManuItems.filter((menuItem) => { return menuItem.category === state.selectedMenuCategory })
      return filteredList
    },
    filteredMenuItems: (state) => state.menuItems.filter((item) => { return item.hidden === false })
  }
}
