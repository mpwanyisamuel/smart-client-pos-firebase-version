import { mapGetters } from 'vuex'
// import moment from 'moment'

export default {
  name: 'SalesSammary',

  computed: {
    ...mapGetters('order', [
      'paymentTypes',
      'allOrders',
      'pendingOrders',
      'paymentsToday'
    ])
  },

  methods: {
    totalSale () {
      let amount = 0
      this.paymentsToday.forEach(payment => {
        amount += parseFloat(payment.amount)
      })
      return this.formatNumber(amount)
    },
    // hasSettlement (paymentId) {
    //   return this.paymentsToday.filter((payment) => { return payment.payment === paymentId }).length > 0
    // },
    settlementAmount (paymentId) {
      const payments = this.paymentsToday.filter((payment) => { return payment.payment === paymentId })
      if (payments.length === 0) {
        return 0
      } else {
        let amount = 0
        payments.forEach(payment => {
          amount += parseFloat(payment.amount)
        })
        return this.formatNumber(amount)
      }
    }
  }
}
