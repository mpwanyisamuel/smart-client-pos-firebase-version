export default {
  name: 'ComputedMethodsMixin',

  methods: {
    formatNumber (amount, decimalCount = 2, decimal = '.', thousands = ',') {
      if (amount.length <= 0 || amount === undefined) return 0
      return Number((amount).toFixed(1)).toLocaleString()
    }
  }
}
