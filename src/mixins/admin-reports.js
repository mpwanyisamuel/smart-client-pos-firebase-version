import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters('order', ['pendingOrders',
      'allOrders',
      'settledOrders',
      'dashboardPaymentOverview',
      'allPayments',
      'creditSettlement',
      'clients',
      'creditors']),
    ...mapGetters('sections', ['outlets']),
    ...mapGetters('auth', ['company']),
    ...mapGetters('manage', ['employees'])
  },

  methods: {

    paymentSettledSammary (paymentId) {
      const payments = this.allPayments.filter(Payment => { return Payment.payment.match(paymentId) })
      let amount = 0
      payments.forEach(payment => {
        amount += parseFloat(payment.amount)
      })
      return amount
    },

    getClientName (clientId) {
      const Client = this.clients.find(Client => { return Client.id === clientId })
      if (Client) return `${Client.firstname} ${Client.lastname}`
      else return 'Client'
    },

    getOutletName (OutletId) {
      const Outlet = this.outlets.find(Outlet => { return Outlet.id === OutletId })
      if (Outlet) return Outlet.name.toLowerCase().split(' ')[0]
      else return 'Undefined'
    },

    getEmployeeName (EmployeeId) {
      const Employee = this.employees.find(Employee => { return Employee.id === EmployeeId })
      if (Employee) return Employee.name.toLowerCase().split(' ')[0]
      else return ''
    },

    getEmployeeNameInitial (EmployeeId) {
      const Employee = this.employees.find(Employee => { return Employee.id === EmployeeId })
      if (Employee) return Employee.name.toLowerCase().split('')[0]
      else return ''
    }
  }
}
