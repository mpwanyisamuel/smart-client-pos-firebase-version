import { GChart } from 'vue-google-charts'
export default {
  name: 'SalesChart',
  components: {
    GChart
  },
  data () {
    return {
      chartOptions: {
        chart: {
          title: 'Sales Sammary',
          subtitle: 'Settlement sammary'
        }
      }
    }
  },

  computed: {
    chartData: {
      set () {
        const allPayments = []
        this.dashboardPaymentOverview.forEach(Payment => {
          const Amount = this.paymentSettledSammary(Payment.id)
          let PaymentName
          if (Payment.settlement.match('MOBILE MONEY')) {
            PaymentName = 'Mobile'
          } else {
            PaymentName = Payment.settlement
          }
          allPayments.push([PaymentName, parseFloat(Amount)])
        })
        const myData = [
          ['Settlement', 'amount']
        ]
        allPayments.forEach(Payment => {
          myData.push(Payment)
        })
        return myData
      },
      get (value) {
        const allPayments = []
        this.dashboardPaymentOverview.forEach(Payment => {
          const Amount = this.paymentSettledSammary(Payment.id)
          let PaymentName
          if (Payment.settlement.match('MOBILE MONEY')) {
            PaymentName = 'Mobile'
          } else {
            PaymentName = Payment.settlement
          }
          allPayments.push([PaymentName, parseFloat(Amount)])
        })
        const myData = [
          ['Settlement', 'amount']
        ]
        allPayments.forEach(Payment => {
          myData.push(Payment)
        })
        return myData
      }
    }
  }
}
