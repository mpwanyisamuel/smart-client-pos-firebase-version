import Vue from 'vue'
import App from './App.vue'
import * as firebase from 'firebase'
import './registerServiceWorker'
import 'firebaseui/dist/firebaseui.css'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueGoogleCharts from 'vue-google-charts'
import FirebaseSettings from './fbSettings'

Vue.use(VueGoogleCharts)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  created () {
    firebase.initializeApp(FirebaseSettings.fb)
    firebase.analytics()
    firebase.firestore().settings({
      cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
    })
    firebase.firestore().enablePersistence()
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        store.dispatch('auth/authUser', user)
      } else {
        store.dispatch('auth/authUser', null)
      }
    })
  },
  render: h => h(App)
}).$mount('#app')
