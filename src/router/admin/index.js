const routes = {
  path: '/admin/',
  name: 'admin-dashboard',
  component: () => import('@/views/admin/index.vue'),
  meta: {
    title: 'Dashboard - SmartPOS',
    loginRequired: true
  },
  children: [
    // {
    //   path: 'Register',
    //   name: 'Register',
    //   component: () => import('@/views/RegisterCompany.vue'),
    //   meta: {
    //     title: 'Register - SmartPOS'
    //   }
    // }
  ]
}

export default routes
