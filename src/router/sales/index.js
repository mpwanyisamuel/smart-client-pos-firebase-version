const routes = [
  {
    path: '/Sales',
    name: 'Sales',
    component: () => import('@/views/Sales.vue')
  },
  {
    path: '/Menu',
    name: 'menu',
    component: () => import('@/views/Menu.vue')
  },
  {
    path: '/Sections',
    name: 'sections',
    component: () => import('@/views/TablesAndSections.vue')
  },
  {
    path: '/Reports',
    name: 'reports',
    component: () => import('@/views/Reports.vue')
  },
  {
    path: '/Outlets',
    name: 'outlets',
    component: () => import('@/views/Outlets.vue')
  }
]

export default routes
