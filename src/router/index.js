import Vue from 'vue'
import VueRouter from 'vue-router'
import admin from './admin'

Vue.use(VueRouter)

const routes = [
  admin,
  {
    path: '/',
    name: 'login',
    component: () => import('../views/Home.vue'),
    meta: {
      title: 'Home - SmartPOS'
    }
  },
  {
    path: '/Signin/',
    name: 'user-auth',
    component: () => import('@/views/auth/userLogin.vue'),
    meta: {
      title: 'Login - SmartPOS',
      loginRequired: false,
      canDisplayGlobalModals: true
    }
  },
  {
    path: '/Register',
    name: 'Register',
    component: () => import('../views/RegisterCompany.vue')
  },
  {
    path: '/Company',
    name: 'Company',
    component: () => import('../views/scm.vue')
  },
  {
    path: '/Sales',
    name: 'Sales',
    component: () => import('../views/Sales.vue')
  },
  {
    path: '/Menu',
    name: 'menu',
    component: () => import('../views/Menu.vue')
  },
  {
    path: '/Sections',
    name: 'sections',
    component: () => import('../views/TablesAndSections.vue')
  },
  {
    path: '/Reports',
    name: 'reports',
    component: () => import('../views/Reports.vue')
  },
  {
    path: '/Outlets',
    name: 'outlets',
    component: () => import('../views/Outlets.vue')
  },
  {
    path: '/Users',
    name: 'users',
    component: () => import('../views/users.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
/*
router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && (localStorage.getItem('user') === null || localStorage.getItem('user') === undefined)) {
    if (to.name === 'Register') {
      next({ name: 'Register' })
    } else {
      next({ name: 'login' })
    }
  } else { next() }
}) */

export default router
