const routes = {
  path: '/Signin/',
  name: 'user-auth',
  component: () => import('@/views/auth/userLogin.vue'),
  meta: {
    title: 'Login - SmartPOS',
    loginRequired: false,
    canDisplayGlobalModals: true
  },
  children: [
    {
      path: ':Register',
      name: 'Register',
      component: () => import('@/views/RegisterCompany.vue'),
      meta: {
        title: 'Register - SmartPOS'
      }
    },
    {
      path: '/Users',
      name: 'users',
      component: () => import('@/views/users.vue')
    }
  ]
}
// {
//   path: '/Signin',
//   name: 'user-auth',
//   component: () => import('@/views/auth/userLogin.vue')
// },
// {
//   path: '/Register',
//   name: 'Register',
//   component: () => import('@/views/RegisterCompany.vue')
// },
// {
//   path: '/Users',
//   name: 'users',
//   component: () => import('@/views/users.vue')
// }

export default routes
